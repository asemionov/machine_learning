## Machine learning

This is an attempt to create a decent machine learning algorithm.
Based on http://www.millermattson.com/dave/?p=54

This project is not the most optimal solution and created for fun.
Also is intended to learn more about data science, Typescript, and exporting packages to be used in other projects.

### Instalation

At the time of implementation uses

- Node v6.11.4
- Typescript Version 2.5.2

In root folder run command line:

```
npm install
```

### Tests

The tests contan several examples of usage - XOR, line recognition, and Square root.

To run all tests run command in root folder:

```
npm test
```

#### Reading test results

When tests are completed, you will see a list of output data. Note:

- `inputVals` that we pass to learning algorythm
- `targetVals` is what the algorythm is supposed to get
- `out` is what the learning process was able to produce - close to the `targetVals`
- relative `error` of computation
