import { ActivationFunction, activationFunctionById } from "./neuralActivation";

const eta: number = 0.15; // [0.0..1.0] overall net training rate
const alpha: number = 0.5; // [0.0..n] multiplier of last weight change (momentum)

export type Layer = Array<Neuron>;

export class Neuron {
  private m_outputVal: number;
  private m_outputWeights: Connection[] = [];
  private m_myIndex: number;
  private m_gradient: number = 0;
  private m_activationMethod: ActivationFunction;

  public constructor() {}

  public setOutputNum(
    numOutputs: number,
    myIndex: number,
    activationMethod: ActivationFunction
  ) {
    this.m_activationMethod = activationMethod;
    for (let i = 0; i < numOutputs; ++i) {
      this.m_outputWeights.push({
        w: Math.random(),
        dw: 0
      });
    }

    this.m_myIndex = myIndex;
  }

  setNeuronState(state: NeuronState) {
    for (let i = 0; i < state.ow.length; ++i) {
      const weight = state.ow[i];
      this.m_outputWeights.push({
        w: weight.w,
        dw: weight.dw
      });
    }
    this.m_gradient = state.g;
    this.m_myIndex = state.i;
    this.m_activationMethod = activationFunctionById(state.t);
  }

  public setOutputVal(val: number): void {
    this.m_outputVal = val;
  }
  public getOutputVal(): number {
    return this.m_outputVal;
  }

  public feedForward(prevLayer: Layer): void {
    let sum = 0.0;

    // Sum the previous layer's outputs (which are our inputs)
    // Include the bias node from the previous layer.

    for (let n = 0; n < prevLayer.length; ++n) {
      sum +=
        prevLayer[n].getOutputVal() *
        prevLayer[n].m_outputWeights[this.m_myIndex].w;
    }

    // this.m_outputVal = Neuron.transferFunction(sum);
    this.m_outputVal = this.m_activationMethod.equation(sum);
  }

  public calcOutputGradients(targetVal: number) {
    const delta = targetVal - this.m_outputVal;
    this.m_gradient =
      delta * this.m_activationMethod.derivative(this.m_outputVal);
  }

  public calcHiddenGradients(nextLayer: Layer): void {
    const dow = this.sumDOW(nextLayer);
    this.m_gradient =
      dow * this.m_activationMethod.derivative(this.m_outputVal);
  }

  public updateInputWeights(prevLayer: Layer) {
    // The weights to be updated are in the Connection container
    // in the neurons in the preceding layer

    for (let n = 0; n < prevLayer.length; ++n) {
      const neuron = prevLayer[n];
      const oldDeltaWeight = neuron.m_outputWeights[this.m_myIndex].dw;

      const newDeltaWeight =
        // Individual input, magnified by the gradient and train rate:
        eta * neuron.getOutputVal() * this.m_gradient +
        // Also add momentum = a fraction of the previous delta weight;
        alpha * oldDeltaWeight;

      neuron.m_outputWeights[this.m_myIndex].dw = newDeltaWeight;
      neuron.m_outputWeights[this.m_myIndex].w += newDeltaWeight;
    }
  }

  private sumDOW(nextLayer: Layer): number {
    let sum = 0.0;
    // Sum our contributions of the errors at the nodes we feed.
    for (let n = 0; n < nextLayer.length - 1; ++n) {
      sum += this.m_outputWeights[n].w * nextLayer[n].m_gradient;
    }

    return sum;
  }

  private serializeConnections() {
    return this.m_outputWeights.map((item: Connection) => {
      return {
        w: item.w,
        dw: item.dw
      };
    });
  }

  public serialize(): NeuronState {
    return {
      ow: this.serializeConnections(),
      i: this.m_myIndex,
      g: this.m_gradient,
      t: this.m_activationMethod.id
    };
  }
}

// Save data
export interface NeuronState {
  ow: Connection[];
  i: number;
  g: number;
  t: number;
}

interface Connection {
  // weight
  w: number;
  // delta weight
  dw: number;
}
