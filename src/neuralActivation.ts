export interface ActivationFunction {
    equation: (x: number) => number;
    derivative: (x: number) => number;
    id: number;
}

enum ActivationFunctionTypes {
    Tangent = 0,
    LeakyRelu,
    Relu,
    Identity,
}

export function activationFunctionById(
    fId: ActivationFunctionTypes,
): ActivationFunction {
    switch (fId) {
        case ActivationFunctionTypes.Tangent:
            return tangent;
        case ActivationFunctionTypes.LeakyRelu:
            return leakyRelu;
        case ActivationFunctionTypes.Identity:
            return identity;
        case ActivationFunctionTypes.Relu:
        default:
            return relu;
    }
}

const tangent: ActivationFunction = {
    equation: (x: number): number => {
        const a = Math.exp(+x),
            b = Math.exp(-x);
        return a == Infinity ? 1 : b == Infinity ? -1 : (a - b) / (a + b);
    },
    derivative: (x: number): number => {
        return 1.0 - x * x;
    },
    id: ActivationFunctionTypes.Tangent,
};

const reluAlpha = 0.01;
const leakyRelu: ActivationFunction = {
    equation: (x: number): number => {
        return x >= 0 ? x : reluAlpha * x;
    },
    derivative: (x: number): number => {
        return x >= 0 ? 1 : reluAlpha;
    },
    id: ActivationFunctionTypes.LeakyRelu,
};

const relu: ActivationFunction = {
    equation: (x: number): number => {
        return Math.max(0, x);
    },
    derivative: (x: number): number => {
        return x > 0 ? 1 : 0;
    },
    id: ActivationFunctionTypes.Relu,
};

const identity: ActivationFunction = {
    equation: (x: number): number => {
        return x;
    },
    derivative: (x: number): number => {
        return 1;
    },
    id: ActivationFunctionTypes.Identity,
};

export const transferFunction = {
    relu,
    leakyRelu,
    tangent,
    identity,
};
