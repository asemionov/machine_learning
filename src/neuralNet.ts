/*
nice source of knowledge https://towardsdatascience.com/activation-functions-neural-networks-1cbd9f8d91d6
*/

import { ActivationFunction, transferFunction } from "./neuralActivation";
import { Neuron, Layer, NeuronState } from "./neuron";

export type NeuralNetState = NeuronState[][];

export { transferFunction } from "./neuralActivation";

export class NeuralNet {
  public constructor() {}

  setTopology(
    topology: Array<number>,
    activation: ActivationFunction = transferFunction.relu
  ) {
    for (let layerNum = 0; layerNum < topology.length; ++layerNum) {
      const layer: Layer = [];
      this.m_layers.push(layer);
      //if last layer then no outputs, else layer length + 1 bias Neuron
      const numOutputs =
        layerNum === topology.length - 1 ? 0 : topology[layerNum + 1];

      // We have a new layer, now fill it with neurons, and
      // add a bias neuron in each layer
      for (let neuronNum = 0; neuronNum <= topology[layerNum]; ++neuronNum) {
        const neuron: Neuron = new Neuron();
        neuron.setOutputNum(numOutputs, neuronNum, activation);
        layer.push(neuron);
      }
      // Force the bias node's output to 1.0 (it was the last neuron pushed in this layer):
      layer[layer.length - 1].setOutputVal(1.0);
    }
  }

  setNetState(state: NeuralNetState) {
    for (let layerNum = 0; layerNum < state.length; ++layerNum) {
      const layer: Layer = [];
      this.m_layers.push(layer);
      const stateLayer: NeuronState[] = state[layerNum];
      for (let neuronNum = 0; neuronNum < stateLayer.length; ++neuronNum) {
        const neuron: Neuron = new Neuron();
        neuron.setNeuronState(stateLayer[neuronNum]);
        layer.push(neuron);
      }
      layer[layer.length - 1].setOutputVal(1.0);
    }
  }

  public feedForward(inputVals: Array<number>) {
    const correct = inputVals.length == this.m_layers[0].length - 1;
    if (!correct) throw "Feed forward failed";
    // Assign (latch) the input values into the input neurons
    for (let i = 0; i < inputVals.length; ++i) {
      this.m_layers[0][i].setOutputVal(inputVals[i]);
    }

    // forward propagate
    for (let layerNum = 1; layerNum < this.m_layers.length; ++layerNum) {
      let prevLayer: Layer = this.m_layers[layerNum - 1];
      // every Neuron in current layer... feed forward previous layers data
      for (let n = 0; n < this.m_layers[layerNum].length - 1; ++n) {
        this.m_layers[layerNum][n].feedForward(prevLayer);
      }
    }
  }

  public backProp(targetVals: Array<number>) {
    // Calculate overall net error (RMS of output neuron errors)

    const outputLayer = this.m_layers[this.m_layers.length - 1];
    this.m_error = 0.0;
    let delta: number = 0;

    // mean squared error calculation - mse
    for (let n = 0; n < outputLayer.length - 1; ++n) {
      delta = targetVals[n] - outputLayer[n].getOutputVal();
      this.m_error += delta * delta;
    }
    this.m_error /= outputLayer.length - 1; // get average error squared

    //root mean square error - rms
    this.m_error = Math.sqrt(this.m_error);

    // Calculate output layer gradients
    for (let n = 0; n < outputLayer.length - 1; ++n) {
      outputLayer[n].calcOutputGradients(targetVals[n]);
    }

    // Calculate hidden layer gradients
    for (let layerNum = this.m_layers.length - 2; layerNum > 0; --layerNum) {
      const hiddenLayer: Layer = this.m_layers[layerNum];
      const nextLayer: Layer = this.m_layers[layerNum + 1];

      for (let n = 0; n < hiddenLayer.length; ++n) {
        hiddenLayer[n].calcHiddenGradients(nextLayer);
      }
    }

    // For all layers from outputs to first hidden layer,
    // update connection weights

    for (let layerNum = this.m_layers.length - 1; layerNum > 0; --layerNum) {
      let layer: Layer = this.m_layers[layerNum];
      let prevLayer: Layer = this.m_layers[layerNum - 1];

      for (let n = 0; n < layer.length - 1; ++n) {
        layer[n].updateInputWeights(prevLayer);
      }
    }
  }

  public getResults(): Array<number> {
    const resultVals: Array<number> = [];
    const lastLayer = this.m_layers[this.m_layers.length - 1];
    for (let n = 0; n < lastLayer.length - 1; ++n) {
      resultVals.push(lastLayer[n].getOutputVal());
    }
    return resultVals;
  }
  public getRecentAverageError(): number {
    return this.m_error;
  }

  public serialize(): NeuralNetState {
    const netState: NeuralNetState = [];
    for (let l = 0; l < this.m_layers.length; ++l) {
      const layerStates: NeuronState[] = [];
      const layer = this.m_layers[l];
      for (let s = 0; s < layer.length; ++s) {
        const nState: NeuronState = layer[s].serialize();
        layerStates.push(nState);
      }
      netState.push(layerStates);
    }
    return netState;
  }

  private m_layers: Array<Layer> = [];
  private m_error: number;
}
