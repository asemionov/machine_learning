"use strict";
// inspired by http://www.millermattson.com/dave/?p=54
exports.__esModule = true;
var Neuron = /** @class */ (function () {
    function Neuron(numOutputs, myIndex) {
        this.m_outputWeights = [];
        for (var c = 0; c < numOutputs; ++c) {
            this.m_outputWeights.push({
                weight: Neuron.randomWeight(),
                deltaWeight: 0
            });
        }
        this.m_myIndex = myIndex;
    }
    Neuron.prototype.setOutputVal = function (val) {
        this.m_outputVal = val;
    };
    Neuron.prototype.getOutputVal = function () {
        return this.m_outputVal;
    };
    Neuron.prototype.feedForward = function (prevLayer) {
        var sum = 0.0;
        // Sum the previous layer's outputs (which are our inputs)
        // Include the bias node from the previous layer.
        for (var n = 0; n < prevLayer.length; ++n) {
            sum +=
                prevLayer[n].getOutputVal() *
                    prevLayer[n].m_outputWeights[this.m_myIndex].weight;
        }
        this.m_outputVal = Neuron.transferFunction(sum);
    };
    Neuron.prototype.calcOutputGradients = function (targetVal) {
        var delta = targetVal - this.m_outputVal;
        this.m_gradient =
            delta * Neuron.transferFunctionDerivative(this.m_outputVal);
    };
    Neuron.prototype.calcHiddenGradients = function (nextLayer) {
        var dow = this.sumDOW(nextLayer);
        this.m_gradient = dow * Neuron.transferFunctionDerivative(this.m_outputVal);
    };
    Neuron.prototype.updateInputWeights = function (prevLayer) {
        // The weights to be updated are in the Connection container
        // in the neurons in the preceding layer
        for (var n = 0; n < prevLayer.length; ++n) {
            var neuron = prevLayer[n];
            var oldDeltaWeight = neuron.m_outputWeights[this.m_myIndex].deltaWeight;
            var newDeltaWeight = 
            // Individual input, magnified by the gradient and train rate:
            Neuron.eta * neuron.getOutputVal() * this.m_gradient +
                // Also add momentum = a fraction of the previous delta weight;
                Neuron.alpha * oldDeltaWeight;
            neuron.m_outputWeights[this.m_myIndex].deltaWeight = newDeltaWeight;
            neuron.m_outputWeights[this.m_myIndex].weight += newDeltaWeight;
        }
    };
    Neuron.tahn = function (x) {
        var a = Math.exp(+x), b = Math.exp(-x);
        return a == Infinity ? 1 : b == Infinity ? -1 : (a - b) / (a + b);
    };
    Neuron.transferFunction = function (x) {
        return Neuron.tahn(x);
    };
    Neuron.transferFunctionDerivative = function (x) {
        // tanh derivative
        return 1.0 - x * x;
    };
    Neuron.randomWeight = function () {
        return Math.random();
    };
    Neuron.prototype.sumDOW = function (nextLayer) {
        var sum = 0.0;
        // Sum our contributions of the errors at the nodes we feed.
        for (var n = 0; n < nextLayer.length - 1; ++n) {
            sum += this.m_outputWeights[n].weight * nextLayer[n].m_gradient;
        }
        return sum;
    };
    Neuron.eta = 0.15; // [0.0..1.0] overall net training rate
    Neuron.alpha = 0.5; // [0.0..n] multiplier of last weight change (momentum)
    return Neuron;
}());
exports.Neuron = Neuron;
///-----
var Net = /** @class */ (function () {
    function Net(topology) {
        this.m_layers = []; // m_layers[layerNum][neuronNum]
        this.m_recentAverageError = 0;
        var numLayers = topology.length;
        for (var layerNum = 0; layerNum < numLayers; ++layerNum) {
            var layer = [];
            this.m_layers.push(layer);
            var numOutputs = layerNum == topology.length - 1 ? 0 : topology[layerNum + 1];
            // We have a new layer, now fill it with neurons, and
            // add a bias neuron in each layer.
            for (var neuronNum = 0; neuronNum <= topology[layerNum]; ++neuronNum) {
                var neuron = new Neuron(numOutputs, neuronNum);
                layer.push(neuron);
                console.log("Made a Neuron!");
            }
            // Force the bias node's output to 1.0 (it was the last neuron pushed in this layer):
            layer[layer.length - 1].setOutputVal(1.0);
        }
    }
    Net.prototype.feedForward = function (inputVals) {
        var correct = inputVals.length == this.m_layers[0].length - 1;
        // console.log("000", inputVals.length, this.m_layers[0].length - 1)
        if (!correct)
            throw "Feed forward failed";
        // Assign (latch) the input values into the input neurons
        for (var i = 0; i < inputVals.length; ++i) {
            this.m_layers[0][i].setOutputVal(inputVals[i]);
        }
        // forward propagate
        for (var layerNum = 1; layerNum < this.m_layers.length; ++layerNum) {
            var prevLayer = this.m_layers[layerNum - 1];
            for (var n = 0; n < this.m_layers[layerNum].length - 1; ++n) {
                this.m_layers[layerNum][n].feedForward(prevLayer);
            }
        }
    };
    Net.prototype.backProp = function (targetVals) {
        // Calculate overall net error (RMS of output neuron errors)
        var outputLayer = this.m_layers[this.m_layers.length - 1];
        this.m_error = 0.0;
        for (var n = 0; n < outputLayer.length - 1; ++n) {
            var delta = targetVals[n] - outputLayer[n].getOutputVal();
            this.m_error += delta * delta;
        }
        this.m_error /= outputLayer.length - 1; // get average error squared
        this.m_error = Math.sqrt(this.m_error); // RMS
        // Implement a recent average measurement
        this.m_recentAverageError =
            (this.m_recentAverageError * Net.m_recentAverageSmoothingFactor +
                this.m_error) /
                (Net.m_recentAverageSmoothingFactor + 1.0);
        // Calculate output layer gradients
        for (var n = 0; n < outputLayer.length - 1; ++n) {
            outputLayer[n].calcOutputGradients(targetVals[n]);
        }
        // Calculate hidden layer gradients
        for (var layerNum = this.m_layers.length - 2; layerNum > 0; --layerNum) {
            var hiddenLayer = this.m_layers[layerNum];
            var nextLayer = this.m_layers[layerNum + 1];
            for (var n = 0; n < hiddenLayer.length; ++n) {
                hiddenLayer[n].calcHiddenGradients(nextLayer);
            }
        }
        // For all layers from outputs to first hidden layer,
        // update connection weights
        for (var layerNum = this.m_layers.length - 1; layerNum > 0; --layerNum) {
            var layer = this.m_layers[layerNum];
            var prevLayer = this.m_layers[layerNum - 1];
            for (var n = 0; n < layer.length - 1; ++n) {
                layer[n].updateInputWeights(prevLayer);
            }
        }
    };
    Net.prototype.getResults = function (resultVals) {
        resultVals.length = 0;
        var lastLayer = this.m_layers[this.m_layers.length - 1];
        for (var n = 0; n < lastLayer.length - 1; ++n) {
            resultVals.push(lastLayer[n].getOutputVal());
        }
    };
    Net.prototype.getRecentAverageError = function () {
        return this.m_recentAverageError;
    };
    Net.m_recentAverageSmoothingFactor = 100.0; // Number of training samples to average over
    return Net;
}());
exports.Net = Net;
/*

function testSetup(): void {
    let topology: Array<number> = []
    topology.push(3)
    topology.push(2)
    topology.push(1)
    let myNet: Net = new Net(topology)

    let inputVals: Array<number> = []
    myNet.feedForward(inputVals)

    let targetVals: Array<number> = []
    myNet.backProp(targetVals)

    let resultVals: Array<number> = []
    myNet.getResults(resultVals)
    
}
*/
