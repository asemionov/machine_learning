import { TrainSchema, TrainValues } from "../neuralNetCoach";

//NOTE - number of hidden layer nodes is eaqual (Nr.In + NR.Out)*2/3

//this data is nonsense as there is no sense in it buuuuut :D
export function prepareSqrt(): TrainSchema {
  let topology: Array<number> = [1, 1];
  let trainData = [];

  for (let i = 1000; i > 0; i--) {
    let n1 = Math.random();
    let t = Math.sqrt(n1);
    trainData.push({ inputVals: [n1], targetVals: [t] });
  }

  return { topology, trainData };
}

function oneOrZero() {
  return Math.random() > 0.5 ? 1 : 0;
}

export function prepareXor(): TrainSchema {
  let topology: Array<number> = [2, 4, 1];
  let trainData = [];
  for (let i = 4000; i > 0; i--) {
    let n1 = oneOrZero();
    let n2 = oneOrZero();
    let t = n1 || n2;
    trainData.push({ inputVals: [n1, n2], targetVals: [t] });
  }

  return { topology, trainData };
}

/* Line recognition
1)   vertical right
     01
     01

2)   vartical left
     10
     10

3)   diagonal forward - as /
     01
     10

4)   diagonal forward - as \
     10
     01

*/

export function prepareLines(): TrainSchema {
  let topology: Array<number> = [4, 5, 3];
  let trainDataSample = [
    { inputVals: [1, 1, 1, 1], targetVals: [1, 1, 1] }, //full black
    { inputVals: [-1, -1, -1, -1], targetVals: [0, 0, 0] }, //full white
    { inputVals: [1, 1, -1, -1], targetVals: [0, 0, 1] }, //horiz top
    { inputVals: [-1, -1, 1, 1], targetVals: [1, 0, 0] }, //horiz bottom

    { inputVals: [1, -1, 1, -1], targetVals: [0, 1, 0] }, //vertical left
    { inputVals: [-1, 1, -1, 1], targetVals: [1, 0, 1] }, //vertical right

    { inputVals: [1, -1, -1, 1], targetVals: [1, 0, 0] }, //diagonal down
    { inputVals: [-1, 1, 1, -1], targetVals: [0, 1, 1] } //diagonal up
  ];

  let trainData: Array<TrainValues> = [];
  for (let i = 0; i < 2000; ++i) {
    trainDataSample.forEach(({ inputVals, targetVals }) =>
      trainData.push({ inputVals, targetVals })
    );
  }

  return { topology, trainData };
}
