import { NeuralNet } from "../src/neuralNet";

const DEFAULT_ELEMENTS_TO_DISPLAY = 3;

export enum TrainPrintMode {
  First = 1,
  Last = -1
}

export interface TrainValues {
  inputVals: Array<number>;
  targetVals: Array<number>;
}

export interface TrainSchema {
  trainData: Array<TrainValues>;
  topology: Array<number>;
}

export interface TrainResults {
  id: number;
  value: Array<number>;
  error: number;
}

export class NeuralNetCoach {
  private m_net: NeuralNet;
  private m_trainData: TrainSchema;

  public constructor(neuralNet: NeuralNet, trainData: TrainSchema) {
    this.m_net = neuralNet;
    this.m_trainData = trainData;
  }

  public train() {
    for (let i = 0; i < this.m_trainData.trainData.length; i++) {
      // Get new input data and feed it forward:
      const { inputVals, targetVals } = this.m_trainData.trainData[i];
      this.m_net.feedForward(inputVals);

      // Train the net what the outputs should have been:
      this.m_net.backProp(targetVals);
    }
  }
}
