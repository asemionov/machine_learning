// https://towardsdatascience.com/activation-functions-neural-networks-1cbd9f8d91d6
import { NeuralNet } from '../src/neuralNet';
import {
    TrainValues,
    TrainSchema,
    NeuralNetCoach,
    TrainPrintMode,
} from './neuralNetCoach';
import {
    prepareSqrt,
    prepareXor,
    prepareLines,
} from './dataCollection/testData';
import { ActivationFunction, transferFunction } from '../src/neuralActivation';

import { expect } from 'chai';

function train(data: TrainSchema, activation: ActivationFunction): NeuralNet {
    const network: NeuralNet = new NeuralNet();
    network.setTopology(data.topology, activation);
    const nc = new NeuralNetCoach(network, data);
    nc.train();
    return network;
}

describe('Neural network', () => {
    it('should complete square root', () => {
        train(prepareSqrt(), transferFunction.relu);
    });

    it('should complete XOR check', () => {
        train(prepareXor(), transferFunction.tangent);
    });

    it('should copy network and repeat prediction', () => {
        const lineData = prepareLines();
        const network = train(lineData, transferFunction.tangent);
        const brain = network.serialize();
        const originalResults: number[] = network.getResults();

        const derivedNetwork: NeuralNet = new NeuralNet();
        derivedNetwork.setNetState(brain);
        // get last training data input / output
        const { inputVals, targetVals } = lineData.trainData[
            lineData.trainData.length - 1
        ];
        derivedNetwork.feedForward(inputVals);
        const derivedResults: number[] = derivedNetwork.getResults();

        let original: string;
        let derived: string;
        for (let i = 0; i < targetVals.length; ++i) {
            original = Math.abs(targetVals[i] / originalResults[i]).toPrecision(
                3,
            );
            derived = Math.abs(targetVals[i] / derivedResults[i]).toPrecision(
                3,
            );
            expect(original).to.equal(derived);
        }
    });
});
