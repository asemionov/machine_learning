module.exports = {
  entry: "./src/neuralNet",
  output: {
    path: __dirname + "/dist",
    filename: "ml.js",
    library: "ml",
    libraryTarget: "umd"
  },
  resolve: {
    extensions: [".ts"]
  },
  module: {
    rules: [{ test: /\.ts?$/, loader: "ts-loader" }]
  }
};
